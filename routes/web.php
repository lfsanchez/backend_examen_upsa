<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('/problem-1', [Controller::class,"league"]);
Route::post('/problem-2', [Controller::class,"chess"]);
Route::post('/problem-3', [Controller::class,"word"]);
