<?php

namespace App\Http\Controllers;


use mysql_xdevapi\Result;

class Functions
{
    public function league($input){
        $data =  explode("\n",$input);
        $league = [];
        $category = "";
        foreach($data as $row){
            if(mb_strtolower($row)!="fin"){
                $set = explode(" ", $row);
                if(sizeof($set)>1){
                    foreach ($set as $value){
                        if(!is_numeric($value)&&!isset($league[$category]["Teams"][ucwords($value)])){
                            $league[$category]["Teams"][ucwords($value)] = ["Name" => ucwords($value), "Points" => 0];
                        }
                    }
                    array_push($league[$category]["Matches"],["Visitor" => ucwords($set[2]),"Local" => ucwords($set[0])]);
                    if($set[1]>$set[3]){
                        $league[$category]["Teams"][ucwords($set[0])]["Points"] += 2;
                        $league[$category]["Teams"][ucwords($set[2])]["Points"] += 1;
                    }
                    else{
                        $league[$category]["Teams"][ucwords($set[2])]["Points"] += 2;
                        $league[$category]["Teams"][ucwords($set[0])]["Points"] += 1;
                    }
                }
                else{
                    if(!isset($league[$row])){
                        $league[$row] = [];
                        $category = $row;
                        $league[$category]["NonPlayed"] = 0;
                        $league[$category]["Name"] = $category;
                        $league[$category]["Teams"] = [];
                        $league[$category]["Matches"] = [];
                    }
                }
            }
            else{
                $league[$category]["NonPlayed"] = (sizeof($league[$category]["Teams"])*(sizeof($league[$category]["Teams"])-1))-sizeof($league[$category]["Matches"]);
            }
        }
        $results = [];
        foreach($league as $category) {
            $max_points = 0;
            $team_name = "";
            foreach ($league[$category["Name"]]["Teams"] as $team) {
                if ($team["Points"] > $max_points) {
                    $max_points = $team["Points"];
                    $team_name = $team["Name"];
                }
                elseif ($team["Points"] == $max_points){
                    $team_name = "EMPATE";
                }
            }
            $results[$category["Name"]] = ["Name" => $category["Name"], "Winner" => $team_name, "NonPlayed" => $league[$category["Name"]]["NonPlayed"]];
        }
        return json_encode($results);
    }

    public function chess($input){
        $data =  explode("\n",$input);
        $dimensions = 0;
        $X_queen = 0;
        $Y_queen = 0;
        $obstacles = array();
        $N_obstacles = 0;
        foreach($data as $row){
            $elements = explode(" ", $row);
            if($dimensions == 0){
                $dimensions = $elements[0];
                $N_obstacles = $elements[1];
            }
            elseif($X_queen == 0 && $Y_queen == 0 && $dimensions > 0){
                $X_queen = $elements[0];
                $Y_queen = $elements [1];
            }
            else{
                array_push($obstacles,["X" => $elements[0], "Y" => $elements[1]]);
            }
        }
        if(sizeof($obstacles) == $N_obstacles){
            return json_encode(["Result" => $this->mov_down($obstacles, $X_queen, $Y_queen) + $this->mov_up($obstacles, $X_queen, $Y_queen, $dimensions) + $this->mov_left($obstacles, $X_queen, $Y_queen) + $this->mov_right($obstacles, $X_queen, $Y_queen, $dimensions) + $this->mov_left_low_diagonal($obstacles, $X_queen, $Y_queen) + $this->mov_left_high_diagonal($obstacles, $X_queen, $Y_queen,$dimensions) + $this->mov_right_high_diagonal($obstacles, $X_queen, $Y_queen,$dimensions) + $this->mov_right_low_diagonal($obstacles, $X_queen, $Y_queen,$dimensions)]);
        }
        elseif(sizeof($obstacles) > $N_obstacles){
            return "Bad format, we have more obstacles than number of obstacles specified";
        }
        else{
            return "Bad format, we have more number of obstacles than obstacles specified";
        }
    }

    function mov_up($obstacles, $X, $Y, $dimensions){
        $count = 0;
        $first_obstacle = $dimensions + 1;
        foreach($obstacles as $obstacle){
            if($obstacle['X'] > $X && $first_obstacle == $dimensions + 1 && $obstacle["Y"] == $Y){
                $first_obstacle = $obstacle['X'];
            }
        }
        $X++;
        while($X <= $dimensions && $X < $first_obstacle){
            $count++;
            $X++;
        }
        return $count;
    }

    function mov_down($obstacles, $X, $Y){
        $count = 0;
        $first_obstacle = 0;
        foreach($obstacles as $obstacle){
            if($obstacle['X'] < $X && $first_obstacle == 0 && $obstacle["Y"] == $Y){
                $first_obstacle = $obstacle['X'];
            }
        }
        $X--;
        while($X > 0 && $X > $first_obstacle){
            $count++;
            $X--;
        }
        return $count;
    }

    function mov_left($obstacles, $X, $Y){
        $count = 0;
        $first_obstacle = 0;
        foreach($obstacles as $obstacle){
            if($obstacle['X'] == $X && $first_obstacle == 0 && $obstacle["Y"] < $Y){
                $first_obstacle = $obstacle['Y'];
            }
        }
        $Y--;
        while($Y > 0 && $Y > $first_obstacle){
            $count++;
            $Y--;
        }
        return $count;
    }

    function mov_right($obstacles, $X, $Y, $dimensions){
        $count = 0;
        $first_obstacle = $dimensions + 1;
        foreach($obstacles as $obstacle){
            if($obstacle['X'] == $X && $first_obstacle == $dimensions + 1 && $obstacle["Y"] > $Y){
                $first_obstacle = $obstacle['Y'];
            }
        }
        $Y++;
        while($Y <= $dimensions && $Y < $first_obstacle){
            $count++;
            $Y++;
        }
        return $count;
    }

    function mov_right_high_diagonal($obstacles, $X, $Y, $dimensions){
        $count = 0;
        $is_obstacle = false;
        $X++;
        $Y++;
        while($X <= $dimensions && $Y <= $dimensions && !$is_obstacle){
            foreach($obstacles as $obstacle){
                if($obstacle['X'] == $X + 1 && $obstacle["Y"] == $Y + 1){
                    $is_obstacle = true;
                }
            }
            if(!$is_obstacle){
                $count++;
            }
            $X++;
            $Y++;
        }
        return $count;
    }

    function mov_right_low_diagonal($obstacles, $X, $Y, $dimensions){
        $count = 0;
        $is_obstacle = false;
        $X--;
        $Y++;
        while($X > 0 && $Y <= $dimensions && !$is_obstacle){
            foreach($obstacles as $obstacle){
                if($obstacle['X'] == $X - 1 && $obstacle["Y"] == $Y + 1){
                    $is_obstacle = true;
                }
            }
            if(!$is_obstacle){
                $count++;
            }
            $X--;
            $Y++;
        }
        return $count;
    }

    function mov_left_high_diagonal($obstacles, $X, $Y, $dimensions){
        $count = 0;
        $is_obstacle = false;
        $X++;
        $Y--;
        while($X <= $dimensions && $Y > 0 && !$is_obstacle){
            foreach($obstacles as $obstacle){
                if($obstacle['X'] == $X + 1 && $obstacle["Y"] == $Y - 1){
                    $is_obstacle = true;
                }
            }
            if(!$is_obstacle){
                $count++;
            }
            $X++;
            $Y--;
        }
        return $count;
    }

    function mov_left_low_diagonal($obstacles, $X, $Y){
        $count = 0;
        $is_obstacle = false;
        $X--;
        $Y--;
        while($X > 0 && $Y > 0 && !$is_obstacle){
            foreach($obstacles as $obstacle){
                if($obstacle['X'] == $X - 1 && $obstacle["Y"] == $Y - 1){
                    $is_obstacle = true;
                }
            }
            if(!$is_obstacle){
                $count++;
            }
            $X--;
            $Y--;
        }
        return $count;
    }

    public function word($input){
        $values = array();
        for($i=0; $i < strlen($input); $i++){
            for($j=1; $j <= strlen($input); $j++){
                array_push($values,mb_strtolower(substr(mb_strtolower($input), $i, $j)));
            }
        }
        $values = array_unique($values);
        $max_result = 0;
        foreach($values as $value){
            $actual = $this->f(mb_strtolower($input), mb_strtolower($value));
            if($actual > $max_result){
                $max_result = $actual;
            }
        }
        return json_encode(["Result" => $max_result]);
    }

    function f($word, $substring){
        $total = 0;
        for($i=0; $i < strlen($word); $i++){
            $sub = substr($word, $i, strlen($substring));
            if($sub == $substring){
                $total++;
            }
        }
        return $total*strlen($substring);
    }
}
