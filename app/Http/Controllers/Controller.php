<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function league(){
        $functions = new Functions();
        return $functions->league($_POST['input']);
    }

    public function chess(){
        $functions = new Functions();
        return $functions->chess($_POST['input']);
    }

    public function word(){
        $functions = new Functions();
        return $functions->word($_POST['input']);
    }
}
